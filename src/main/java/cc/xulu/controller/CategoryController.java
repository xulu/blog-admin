package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class CategoryController extends Controller {
	public void index() {
		setAttr("categoryPage", Category.dao.paginate(getParaToInt(0, 1), 10, "select *", "from category order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    render("edit.html");
	}

    public void edit() {
        setAttr("category", Category.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		Category category = getModel(Category.class);

		category.save();

        renderJson(category);
	}

    public void show() {
        Category category = Category.dao.findById(getParaToInt());

        renderJson(category);

    }
	
	public void update() {
        Category category = getModel(Category.class);
        category.update();
        renderJson(new Status());
	}
	
	public void delete() {
        Category.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}
