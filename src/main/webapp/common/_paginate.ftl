<#macro paginate currentPage totalPage actionUrl urlParas="">
	<#if (totalPage <= 0) || (currentPage > totalPage)><#return></#if>
	<#local startPage = currentPage - 4>
	<#if (startPage < 1)><#local startPage = 1></#if>
	
	<#local endPage = currentPage + 4>
	<#if (endPage > totalPage)><#local endPage = totalPage></#if>
        <div class="ui pagination menu">
		<#if (currentPage <= 8)>
			<#local startPage = 1>
		</#if>
		<#if ((totalPage - currentPage) < 8)>
			<#local endPage = totalPage>
		</#if>
		<ul>
            <a class="icon item">
                <i class="icon left arrow"></i>
            </a>
			<#if (currentPage > 8)>
                <a class="item">
                    #{1}
                </a>
                <a class="item">
                    #{2}
                </a>
                <div class="disabled item">
                    ...
                </div>
			</#if>
			
			<#list startPage..endPage as i>
				<#if currentPage == i>
                    <a class="active item">
                        #{i}
                    </a>
				<#else>
                    <a class="item">
                        #{i}
                    </a>
				</#if>
			</#list>
			
			<#if ((totalPage - currentPage) >= 8)>
                <div class="disabled item">
                    ...
                </div>
                <a class="item">
                    #{totalPage - 1}
                </a>
                <a class="item">
                    #{totalPage}
                </a>

			</#if>

            <a class="icon item">
                <i class="icon right arrow"></i>
            </a>
		</ul>
	    </div>
</#macro>